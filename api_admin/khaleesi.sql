--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.5
-- Dumped by pg_dump version 9.4.5
-- Started on 2017-06-11 15:56:39

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 231 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2338 (class 0 OID 0)
-- Dependencies: 231
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 172 (class 1259 OID 17014)
-- Name: api_estilos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE api_estilos (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion character varying(900) NOT NULL,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE api_estilos OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 17020)
-- Name: api_estilos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_estilos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_estilos_id_seq OWNER TO postgres;

--
-- TOC entry 2339 (class 0 OID 0)
-- Dependencies: 173
-- Name: api_estilos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_estilos_id_seq OWNED BY api_estilos.id;


--
-- TOC entry 174 (class 1259 OID 17022)
-- Name: api_preguntas_estilos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE api_preguntas_estilos (
    id integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    estilo_id integer,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE api_preguntas_estilos OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 17025)
-- Name: api_preguntas_estilos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_preguntas_estilos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_preguntas_estilos_id_seq OWNER TO postgres;

--
-- TOC entry 2340 (class 0 OID 0)
-- Dependencies: 175
-- Name: api_preguntas_estilos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_preguntas_estilos_id_seq OWNED BY api_preguntas_estilos.id;


--
-- TOC entry 176 (class 1259 OID 17027)
-- Name: api_preguntas_ocasiones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE api_preguntas_ocasiones (
    descripcion character varying(100) NOT NULL,
    ocasiones_id integer,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE api_preguntas_ocasiones OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 17030)
-- Name: api_usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE api_usuario (
    id integer NOT NULL,
    usuario character varying(50) NOT NULL,
    password character varying(60) NOT NULL,
    sexo character varying(1),
    id_piel integer DEFAULT 1 NOT NULL,
    correo character varying(50),
    telefono character varying(15),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE api_usuario OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 17034)
-- Name: api_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_usuario_id_seq OWNER TO postgres;

--
-- TOC entry 2341 (class 0 OID 0)
-- Dependencies: 178
-- Name: api_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_usuario_id_seq OWNED BY api_usuario.id;


--
-- TOC entry 179 (class 1259 OID 17036)
-- Name: app_historico; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE app_historico (
    id integer NOT NULL,
    tabla character varying(50) NOT NULL,
    concepto character varying(50) NOT NULL,
    idregistro character varying(50) NOT NULL,
    usuario character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE app_historico OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 17039)
-- Name: app_historico_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE app_historico_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_historico_id_seq OWNER TO postgres;

--
-- TOC entry 2342 (class 0 OID 0)
-- Dependencies: 180
-- Name: app_historico_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE app_historico_id_seq OWNED BY app_historico.id;


--
-- TOC entry 181 (class 1259 OID 17041)
-- Name: app_perfil; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE app_perfil (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE app_perfil OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 17044)
-- Name: app_perfil_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE app_perfil_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_perfil_id_seq OWNER TO postgres;

--
-- TOC entry 2343 (class 0 OID 0)
-- Dependencies: 182
-- Name: app_perfil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE app_perfil_id_seq OWNED BY app_perfil.id;


--
-- TOC entry 183 (class 1259 OID 17046)
-- Name: app_perfiles_permisos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE app_perfiles_permisos (
    perfil_id integer NOT NULL,
    ruta character varying(200) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE app_perfiles_permisos OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 17049)
-- Name: app_preguntas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE app_preguntas (
    id integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE app_preguntas OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 17052)
-- Name: app_preguntas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE app_preguntas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_preguntas_id_seq OWNER TO postgres;

--
-- TOC entry 2344 (class 0 OID 0)
-- Dependencies: 185
-- Name: app_preguntas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE app_preguntas_id_seq OWNED BY app_preguntas.id;


--
-- TOC entry 186 (class 1259 OID 17054)
-- Name: app_usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE app_usuario (
    id integer NOT NULL,
    usuario character varying(50) NOT NULL,
    password character varying(60) NOT NULL,
    dni integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(100),
    id_piel integer DEFAULT 1 NOT NULL,
    correo character varying(50),
    telefono character varying(15),
    foto character varying(255) DEFAULT 'user.png'::character varying NOT NULL,
    perfil_id integer,
    autenticacion character(1) DEFAULT 'l'::bpchar NOT NULL,
    super character(1) DEFAULT 'n'::bpchar NOT NULL,
    sexo character varying(1),
    edo_civil character varying(2),
    direccion character varying(200),
    facebook character varying(200),
    instagram character varying(200),
    twitter character varying(200),
    preguntas_pri_id integer,
    preguntas_seg_id integer,
    respuesta_pri character varying(200),
    respuesta_seg character varying(200),
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE app_usuario OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 17064)
-- Name: app_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE app_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_usuario_id_seq OWNER TO postgres;

--
-- TOC entry 2345 (class 0 OID 0)
-- Dependencies: 187
-- Name: app_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE app_usuario_id_seq OWNED BY app_usuario.id;


--
-- TOC entry 188 (class 1259 OID 17066)
-- Name: app_usuario_permisos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE app_usuario_permisos (
    usuario_id integer NOT NULL,
    ruta character varying(200) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE app_usuario_permisos OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 17069)
-- Name: cat_colores; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cat_colores (
    id integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE cat_colores OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 17072)
-- Name: cat_colores_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cat_colores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cat_colores_id_seq OWNER TO postgres;

--
-- TOC entry 2346 (class 0 OID 0)
-- Dependencies: 190
-- Name: cat_colores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cat_colores_id_seq OWNED BY cat_colores.id;


--
-- TOC entry 191 (class 1259 OID 17074)
-- Name: colores_hexa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE colores_hexa (
    id integer NOT NULL,
    cat_colores_id integer,
    descripcion character varying(100) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE colores_hexa OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 17077)
-- Name: colores_hexa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE colores_hexa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE colores_hexa_id_seq OWNER TO postgres;

--
-- TOC entry 2347 (class 0 OID 0)
-- Dependencies: 192
-- Name: colores_hexa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE colores_hexa_id_seq OWNED BY colores_hexa.id;


--
-- TOC entry 193 (class 1259 OID 17079)
-- Name: colores_imponen_estacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE colores_imponen_estacion (
    id integer NOT NULL,
    descripcion character varying(10) NOT NULL,
    estaciones_id integer,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE colores_imponen_estacion OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 17082)
-- Name: colores_imponen_estacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE colores_imponen_estacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE colores_imponen_estacion_id_seq OWNER TO postgres;

--
-- TOC entry 2348 (class 0 OID 0)
-- Dependencies: 194
-- Name: colores_imponen_estacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE colores_imponen_estacion_id_seq OWNED BY colores_imponen_estacion.id;


--
-- TOC entry 195 (class 1259 OID 17084)
-- Name: config_colores_prendas_princ; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE config_colores_prendas_princ (
    config_combina_id integer,
    hexadecimal character varying(7),
    r character varying(3),
    g character varying(3),
    b character varying(3)
);


ALTER TABLE config_colores_prendas_princ OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 17087)
-- Name: config_colores_prendas_sec; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE config_colores_prendas_sec (
    config_combina_id integer,
    tipo_prenda_id integer,
    hexadecimal character varying(7),
    r character varying(3),
    g character varying(3),
    b character varying(3)
);


ALTER TABLE config_colores_prendas_sec OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 17090)
-- Name: config_combinaciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE config_combinaciones (
    id integer NOT NULL,
    prenda_princ_id integer,
    descripcion character varying(255),
    created_at time with time zone,
    deleted_at time with time zone,
    updated_at time with time zone
);


ALTER TABLE config_combinaciones OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 17093)
-- Name: config_combinaciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE config_combinaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE config_combinaciones_id_seq OWNER TO postgres;

--
-- TOC entry 2349 (class 0 OID 0)
-- Dependencies: 198
-- Name: config_combinaciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE config_combinaciones_id_seq OWNED BY config_combinaciones.id;


--
-- TOC entry 199 (class 1259 OID 17095)
-- Name: config_estacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE config_estacion (
    config_combina_id integer,
    estaciones_id integer
);


ALTER TABLE config_estacion OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 17098)
-- Name: config_ocasiones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE config_ocasiones (
    config_combina_id integer,
    ocasiones_id integer
);


ALTER TABLE config_ocasiones OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 17101)
-- Name: config_sexo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE config_sexo (
    config_combina_id integer,
    sexo character varying(1)
);


ALTER TABLE config_sexo OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 17104)
-- Name: config_tipos_prendas_princ_detalle; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE config_tipos_prendas_princ_detalle (
    config_combina_id integer,
    prenda_princ_id integer,
    tipo_prenda_detalle_id integer
);


ALTER TABLE config_tipos_prendas_princ_detalle OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 17107)
-- Name: config_tipos_prendas_sec_detalle; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE config_tipos_prendas_sec_detalle (
    config_combina_id integer,
    prenda_sec_id integer,
    tipo_prenda_detalle_id integer
);


ALTER TABLE config_tipos_prendas_sec_detalle OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 17110)
-- Name: config_tono_piel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE config_tono_piel (
    config_combina_id integer,
    tono_piel_id integer
);


ALTER TABLE config_tono_piel OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 17113)
-- Name: estaciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estaciones (
    id integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    estatus character varying(1) NOT NULL,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE estaciones OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 17116)
-- Name: estaciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE estaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE estaciones_id_seq OWNER TO postgres;

--
-- TOC entry 2350 (class 0 OID 0)
-- Dependencies: 206
-- Name: estaciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE estaciones_id_seq OWNED BY estaciones.id;


--
-- TOC entry 207 (class 1259 OID 17118)
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 17121)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2351 (class 0 OID 0)
-- Dependencies: 208
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- TOC entry 209 (class 1259 OID 17123)
-- Name: ocasiones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ocasiones (
    id integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE ocasiones OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 17126)
-- Name: ocasiones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ocasiones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ocasiones_id_seq OWNER TO postgres;

--
-- TOC entry 2352 (class 0 OID 0)
-- Dependencies: 210
-- Name: ocasiones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ocasiones_id_seq OWNED BY ocasiones.id;


--
-- TOC entry 211 (class 1259 OID 17128)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE password_resets OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 17134)
-- Name: prenda_usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE prenda_usuario (
    id integer NOT NULL,
    usuario_id integer,
    url character varying(100),
    favorito boolean NOT NULL,
    tipo_prenda_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    tipo_prenda_detalle_id integer,
    hexadecimal character varying(7),
    r character varying(3),
    g character varying(3),
    b character varying(3)
);


ALTER TABLE prenda_usuario OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 17137)
-- Name: prenda_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE prenda_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE prenda_usuario_id_seq OWNER TO postgres;

--
-- TOC entry 2353 (class 0 OID 0)
-- Dependencies: 213
-- Name: prenda_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE prenda_usuario_id_seq OWNED BY prenda_usuario.id;


--
-- TOC entry 214 (class 1259 OID 17139)
-- Name: sessions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sessions (
    id character varying(255) NOT NULL,
    user_id integer,
    ip_address character varying(45),
    user_agent text,
    payload text NOT NULL,
    last_activity integer NOT NULL
);


ALTER TABLE sessions OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 17145)
-- Name: telas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE telas (
    id integer NOT NULL,
    descripcion character varying(255),
    estacion_id integer,
    deleted_at time with time zone,
    created_at time with time zone,
    updated_at time with time zone
);


ALTER TABLE telas OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 17148)
-- Name: telas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE telas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE telas_id_seq OWNER TO postgres;

--
-- TOC entry 2354 (class 0 OID 0)
-- Dependencies: 216
-- Name: telas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE telas_id_seq OWNED BY telas.id;


--
-- TOC entry 217 (class 1259 OID 17150)
-- Name: telas_img; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE telas_img (
    telas_id integer,
    url character varying(100),
    nombre character varying(100),
    id integer NOT NULL,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE telas_img OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 17153)
-- Name: telas_img_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE telas_img_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE telas_img_id_seq OWNER TO postgres;

--
-- TOC entry 2355 (class 0 OID 0)
-- Dependencies: 218
-- Name: telas_img_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE telas_img_id_seq OWNED BY telas_img.id;


--
-- TOC entry 219 (class 1259 OID 17155)
-- Name: texturaprenda; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE texturaprenda (
    id integer NOT NULL,
    descripcion character varying(255),
    deleted_at timestamp with time zone,
    updated_at timestamp with time zone,
    created_at timestamp with time zone
);


ALTER TABLE texturaprenda OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 17158)
-- Name: texturaprenda_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE texturaprenda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE texturaprenda_id_seq OWNER TO postgres;

--
-- TOC entry 2356 (class 0 OID 0)
-- Dependencies: 220
-- Name: texturaprenda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE texturaprenda_id_seq OWNED BY texturaprenda.id;


--
-- TOC entry 221 (class 1259 OID 17160)
-- Name: tipo_prenda; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipo_prenda (
    id integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE tipo_prenda OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 17163)
-- Name: tipo_prenda_detalle; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipo_prenda_detalle (
    id integer NOT NULL,
    tipo_prenda_id integer,
    descripcion character varying(255),
    deleted_at time with time zone,
    created_at time with time zone,
    updated_at time with time zone
);


ALTER TABLE tipo_prenda_detalle OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 17166)
-- Name: tipo_prenda_detalle_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipo_prenda_detalle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipo_prenda_detalle_id_seq OWNER TO postgres;

--
-- TOC entry 2357 (class 0 OID 0)
-- Dependencies: 223
-- Name: tipo_prenda_detalle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipo_prenda_detalle_id_seq OWNED BY tipo_prenda_detalle.id;


--
-- TOC entry 224 (class 1259 OID 17168)
-- Name: tipo_prenda_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipo_prenda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipo_prenda_id_seq OWNER TO postgres;

--
-- TOC entry 2358 (class 0 OID 0)
-- Dependencies: 224
-- Name: tipo_prenda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipo_prenda_id_seq OWNED BY tipo_prenda.id;


--
-- TOC entry 225 (class 1259 OID 17170)
-- Name: tipos_prendas_relacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipos_prendas_relacion (
    tipo_prenda_id integer,
    tipo_prenda_id_relacion integer
);


ALTER TABLE tipos_prendas_relacion OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 17173)
-- Name: tono_piel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tono_piel (
    id integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE tono_piel OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 17176)
-- Name: tono_piel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tono_piel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tono_piel_id_seq OWNER TO postgres;

--
-- TOC entry 2359 (class 0 OID 0)
-- Dependencies: 227
-- Name: tono_piel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tono_piel_id_seq OWNED BY tono_piel.id;


--
-- TOC entry 228 (class 1259 OID 17178)
-- Name: turnos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE turnos (
    id integer NOT NULL,
    descripcion character varying(50),
    deleted_at time with time zone,
    updated_at time with time zone,
    created_at time with time zone
);


ALTER TABLE turnos OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 17181)
-- Name: turnos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE turnos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE turnos_id_seq OWNER TO postgres;

--
-- TOC entry 2360 (class 0 OID 0)
-- Dependencies: 229
-- Name: turnos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE turnos_id_seq OWNED BY turnos.id;


--
-- TOC entry 230 (class 1259 OID 17299)
-- Name: votacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE votacion (
    config_combina_id integer,
    usuario_id integer,
    rating integer
);


ALTER TABLE votacion OWNER TO postgres;

--
-- TOC entry 2071 (class 2604 OID 17183)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_estilos ALTER COLUMN id SET DEFAULT nextval('api_estilos_id_seq'::regclass);


--
-- TOC entry 2072 (class 2604 OID 17184)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_preguntas_estilos ALTER COLUMN id SET DEFAULT nextval('api_preguntas_estilos_id_seq'::regclass);


--
-- TOC entry 2074 (class 2604 OID 17185)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_usuario ALTER COLUMN id SET DEFAULT nextval('api_usuario_id_seq'::regclass);


--
-- TOC entry 2075 (class 2604 OID 17186)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_historico ALTER COLUMN id SET DEFAULT nextval('app_historico_id_seq'::regclass);


--
-- TOC entry 2076 (class 2604 OID 17187)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_perfil ALTER COLUMN id SET DEFAULT nextval('app_perfil_id_seq'::regclass);


--
-- TOC entry 2077 (class 2604 OID 17188)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_preguntas ALTER COLUMN id SET DEFAULT nextval('app_preguntas_id_seq'::regclass);


--
-- TOC entry 2082 (class 2604 OID 17189)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_usuario ALTER COLUMN id SET DEFAULT nextval('app_usuario_id_seq'::regclass);


--
-- TOC entry 2083 (class 2604 OID 17190)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cat_colores ALTER COLUMN id SET DEFAULT nextval('cat_colores_id_seq'::regclass);


--
-- TOC entry 2084 (class 2604 OID 17191)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY colores_hexa ALTER COLUMN id SET DEFAULT nextval('colores_hexa_id_seq'::regclass);


--
-- TOC entry 2085 (class 2604 OID 17192)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY colores_imponen_estacion ALTER COLUMN id SET DEFAULT nextval('colores_imponen_estacion_id_seq'::regclass);


--
-- TOC entry 2086 (class 2604 OID 17193)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY config_combinaciones ALTER COLUMN id SET DEFAULT nextval('config_combinaciones_id_seq'::regclass);


--
-- TOC entry 2087 (class 2604 OID 17194)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estaciones ALTER COLUMN id SET DEFAULT nextval('estaciones_id_seq'::regclass);


--
-- TOC entry 2088 (class 2604 OID 17195)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- TOC entry 2089 (class 2604 OID 17196)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ocasiones ALTER COLUMN id SET DEFAULT nextval('ocasiones_id_seq'::regclass);


--
-- TOC entry 2090 (class 2604 OID 17197)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY prenda_usuario ALTER COLUMN id SET DEFAULT nextval('prenda_usuario_id_seq'::regclass);


--
-- TOC entry 2091 (class 2604 OID 17198)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telas ALTER COLUMN id SET DEFAULT nextval('telas_id_seq'::regclass);


--
-- TOC entry 2092 (class 2604 OID 17199)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telas_img ALTER COLUMN id SET DEFAULT nextval('telas_img_id_seq'::regclass);


--
-- TOC entry 2093 (class 2604 OID 17200)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY texturaprenda ALTER COLUMN id SET DEFAULT nextval('texturaprenda_id_seq'::regclass);


--
-- TOC entry 2094 (class 2604 OID 17201)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipo_prenda ALTER COLUMN id SET DEFAULT nextval('tipo_prenda_id_seq'::regclass);


--
-- TOC entry 2095 (class 2604 OID 17202)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipo_prenda_detalle ALTER COLUMN id SET DEFAULT nextval('tipo_prenda_detalle_id_seq'::regclass);


--
-- TOC entry 2096 (class 2604 OID 17203)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tono_piel ALTER COLUMN id SET DEFAULT nextval('tono_piel_id_seq'::regclass);


--
-- TOC entry 2097 (class 2604 OID 17204)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY turnos ALTER COLUMN id SET DEFAULT nextval('turnos_id_seq'::regclass);


--
-- TOC entry 2272 (class 0 OID 17014)
-- Dependencies: 172
-- Data for Name: api_estilos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (1, 'Boho', 'El estilo Boho es desde 2004, año en el que nació, es el más difícil de replicar. Es algo así como un cóctel en el que se mezclan abundantes ecos hippies y bohemios, con un toque étnico –del navajo al arabesco– y otro toque campestre.', NULL, '2017-04-19 23:28:34', '2017-04-19 23:28:34');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (2, 'Hipster', 'Recreando una mirada inconformista el estilo “Hipster” obedece a la forma en cómo algunas mujeres combinan un estilo motorista con el grunge, boho, vagabundo y lo hippie, haciendo algo individual y original.  Las prendas conjugan numerosas ideas de combinación de mezclas, calcetines hasta la rodilla, medias de lunares, tops frescos de rayas y grandes sombreros, etc. El estilo “Hipster” está en las calles, donde las chicas aparecen en prendas de vestir de estilo vintage muy fáciles de poner  y cómodas. En otras palabras, todos estos elementos esenciales son ideales para haciendo que se vea especial y de moda.', NULL, '2017-04-19 23:29:03', '2017-04-19 23:29:03');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (3, 'Trendy', 'El término “Trendy” en el mundo de la moda engloba todo lo que está de moda; por lo que una tendencia puede ser “Trendy” este año, pero quedar desfasado para la próxima temporada. También se aplica a otros campos como la decoración o el interiorismo.', NULL, '2017-04-19 23:29:20', '2017-04-19 23:29:20');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (4, 'Casual', 'La ropa Casual es la ropa informal: es decir, aquella que se utiliza en contextos que no exigen el respeto de un código de vestimenta formal. A la hora de escoger ropa casual para vestirse, por lo tanto, una persona no se ata a criterios rígidos de elegancia ni se preocupa por lograr un aspecto de seriedad. Aunque el estilo “Casual” se considera a todo aquello no solo que te permite vestir de manera cómoda sino también reflejar la personalidad e incluso poder lucir de una forma apropiada tanto para ir a trabajar como para ir de fiesta.', NULL, '2017-04-19 23:29:37', '2017-04-19 23:29:37');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (5, 'Artsy', 'Las mujeres que disfrutan el estilo “Artsy” tienden a permanecer lejos de las tendencias tradicionales del mundo de la moda y a mostrar un  amor incondicional a su propio estilo para hacer una declaración con su ropa. A menudo van a ser ellas las creadoras de sus propias modas, diseño y creación de sus propias blusas, sombreros y chaquetas. Cada estilo artístico será diferente por mujer, ya que cada uno tiene su propia idea de lo “arte” es en realidad. Eso es lo que hace que este estilo de la manera particular, tan poco convencional y tan interesante.', NULL, '2017-04-19 23:29:56', '2017-04-19 23:29:56');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (6, 'Clásico', 'El estilo Classic proyecta la imagen de un hombre o una mujer equilibrada y confiable, con buena educación y cultura, aferrada a tradiciones y valores algo estrictos, alejada de la frivolidad y algo seria. Sus colores son los neutros como azul, café, burdeo, beige, verde musgo, gris y negro, en combinaciones tono sobre tono y convencionales. Las texturas de las telas favoritas de una mujer clásica, son los tejidos planos o con relieves acanalados y afranelados. Para fiestas las texturas algo satinadas.', NULL, '2017-04-19 23:30:36', '2017-04-19 23:30:36');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (7, 'Exótico', 'El estilo “Exótico” surge de la constante exposición a otras culturas que está creando una fascinante fusión de la moda occidental y oriental. materiales de caída, gasas y sedas, colores brillantes y diseños florales intrincados inspirado por la elaborada decoración interior de la arquitectura famosa del mundo en países como India y Marruecos han sido cosidos juntos y se puede ver pavoneándose por pasarelas. Vogue lo describió como “futurismo oriental para la mujer moderna ‘', NULL, '2017-04-19 23:30:50', '2017-04-19 23:30:50');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (8, 'Glamoroso', 'Glamorous designa un estilo en la forma de vestir con características atractivas de una determinada época, con un estilo una belleza intrínsica marcando la estética, el exceso, la vanidad y la atracción sexual.', NULL, '2017-04-19 23:31:05', '2017-04-19 23:31:05');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (9, 'Romántico', 'El Romántico tiene la característica de ser un estilo bastante más complejo y con mayor estructura que otros estilos. Actualmente está a la moda, aunque siempre tuvo su importancia y es muy usado en esta época donde la simplicidad es un factor común entre nosotros. El romanticismo tiene la característica de exigir gran decoración la cual muchas veces llega a ser excesiva. Es destacada la ornamentación en las superficies de las telas que caracteriza a este estilo. En lo que respecta a las telas también se puede destacar el color, la impresión abundante y los efectos tridimensionales en los cortes.', NULL, '2017-04-19 23:31:20', '2017-04-19 23:31:20');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (10, 'Sexy', 'Hay dos objetivos principales del estilo “Sexy” llamar la atención de todos los hombres a su alrededor y mostrar tanta piel como sea legal y humanamente posible. El estilo Sexy tiene que ver con mostrar sus mejores características: los pechos, el estómago y las piernas. Una mujer cuyo estilo de la moda se establece en sexy es generalmente cargado con un montón de minifaldas, vestidos de cuerpo-con, tacones altos, y blusas o tops escotados.', NULL, '2017-04-19 23:31:36', '2017-04-19 23:31:36');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (11, 'Sofisticado', 'Todos tenemos alguna amiga así. Las cabezas se giran mientras camina por la calle y se deja un aroma de perfume en su estela. Sus uñas están perfectamente cuidadas, que lleva gafas de sol sobre su cabeza y sus pendientes de diamantes son lo suficientemente pequeño como para ser real, pero lo suficientemente grandes como para hacerse notar.  Puede llevar diferentes estilos de prendas y colores pero sabe combinarlos a la perfección, como piezas de un puzzle que encajan perfectamente. Eso que hace que destaque entre la multitud es precisamente su sofisticación.', NULL, '2017-04-19 23:31:49', '2017-04-19 23:31:49');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (12, 'Streetwear', 'Sobre el origen del Streetwear hay diversas teorías , pero independiente de su origen, hoy en día el Streetwear ha cruzado las fronteras de la ropa y se ha trasladado a distintas áreas de la moda. Gafas de sol y bolsos están cada vez más presentes en los look de sus seguidores y marcas como Eastpak o Blackflyz han comenzado a producir este tipo de accesorios inspirados en el Streetwear.', NULL, '2017-04-19 23:32:08', '2017-04-19 23:32:08');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (13, 'Western', 'El Western es un estilo que deriva de la ropa usada en el siglo XIX en el oeste americano. Se extiende desde la ropa llevada por pioneros, hombre de la montaña, de la guerra civil, y la ropa de vaquero  a prendas estilizadas popularizados por canciones  en los años 1940 y 1950.', NULL, '2017-04-19 23:32:25', '2017-04-19 23:32:25');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (14, 'Preppy', 'La chicas Preppy, o también llamadas ‘Preps’, aman el mecerse en un armario inspirado en tiempos del colegio. Sus perchas están alineadas de lado a lado con blusas femeninas y las camisetas a juego con cuello, así como las faldas del A-line y medias. Su cabello es casi siempre amplificado con una linda diadema y ella suele llevar gafas. Este estilo puede parecer un poco “friki” y un poco de lujo al mismo tiempo, en realidad no es realmente no es caro ni  extravagante en absoluto.', NULL, '2017-04-19 23:32:40', '2017-04-19 23:32:40');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (15, 'Punk', 'El estilo de la moda Punk está al borde del grito y actitud. A los Punkies les gusta usar un montón de cuero– sobre todo cuando se trata de chaquetas y pantalones con pasadores, clavos y cadenas. Tienden a caer también en grabados spandex y animales silvestres, así como de la banda camisetas, botas y jeans ajustados.', NULL, '2017-04-19 23:32:55', '2017-04-19 23:32:55');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (16, 'Rock', 'El estilo Rocker es muy versátil y va a cambiar de persona a persona, dependiendo del tipo particular de rock que aman. Sin embargo, si estás luciendo una camiseta “Ramones” o una sudadera Metálica, tendrás unas cuantas cosas en común: vaqueros rasgados, botas, chaquetas de cuero y los pantalones, y clavos.', NULL, '2017-04-19 23:33:10', '2017-04-19 23:33:10');
INSERT INTO api_estilos (id, nombre, descripcion, deleted_at, created_at, updated_at) VALUES (17, 'Gótico', 'Sólo hay una cosa que está omnipresente en el estilo Gótico:  “lo negro”. Todo lo relacionado con el estilo gótico es negro; de pelo negro con los labios negros, camisas negras, las botas negras. Las mujeres que llevan modas góticas típicamente pueden ver el uso de ropa muy ajustada, vestidos negros intrincados, y un montón de cadenas, clavos, tacos, y otros estilos de accesorios exóticos. El aspecto general está diseñado para decir “mórbida” y “misterioso”, y que se logra fácilmente con la ropa muy oscura y accesorios de pies a cabeza.', NULL, '2017-04-19 23:33:34', '2017-04-19 23:33:34');


--
-- TOC entry 2361 (class 0 OID 0)
-- Dependencies: 173
-- Name: api_estilos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_estilos_id_seq', 17, true);


--
-- TOC entry 2274 (class 0 OID 17022)
-- Dependencies: 174
-- Data for Name: api_preguntas_estilos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO api_preguntas_estilos (id, descripcion, estilo_id, deleted_at, created_at, updated_at) VALUES (2, 'Eres un soñador?', 1, NULL, '2017-04-19 23:59:33', '2017-04-19 23:59:33');
INSERT INTO api_preguntas_estilos (id, descripcion, estilo_id, deleted_at, created_at, updated_at) VALUES (3, 'Eres amante de las artes?', 1, NULL, '2017-04-20 00:00:56', '2017-04-20 00:00:56');
INSERT INTO api_preguntas_estilos (id, descripcion, estilo_id, deleted_at, created_at, updated_at) VALUES (5, 'Usas el Cabello largo y despreocupado?', 2, NULL, '2017-04-20 00:04:44', '2017-04-20 00:04:44');
INSERT INTO api_preguntas_estilos (id, descripcion, estilo_id, deleted_at, created_at, updated_at) VALUES (6, 'Usas ropa nueva que parece vintage?', 2, NULL, '2017-04-20 00:05:28', '2017-04-20 00:05:28');
INSERT INTO api_preguntas_estilos (id, descripcion, estilo_id, deleted_at, created_at, updated_at) VALUES (7, 'Te consideras una persona conservadora en tu forma de vestir?', 4, NULL, '2017-04-20 00:11:19', '2017-04-20 00:13:22');
INSERT INTO api_preguntas_estilos (id, descripcion, estilo_id, deleted_at, created_at, updated_at) VALUES (8, 'Usas prendas clásicas?', 4, NULL, '2017-04-20 00:14:38', '2017-04-20 00:14:38');
INSERT INTO api_preguntas_estilos (id, descripcion, estilo_id, deleted_at, created_at, updated_at) VALUES (4, 'Te gusta usar jeans angostos (pitillo)?', 2, NULL, '2017-04-20 00:03:11', '2017-04-20 00:03:11');


--
-- TOC entry 2362 (class 0 OID 0)
-- Dependencies: 175
-- Name: api_preguntas_estilos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_preguntas_estilos_id_seq', 8, true);


--
-- TOC entry 2276 (class 0 OID 17027)
-- Dependencies: 176
-- Data for Name: api_preguntas_ocasiones; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO api_preguntas_ocasiones (descripcion, ocasiones_id, deleted_at, created_at, updated_at) VALUES ('La Recepción es en el Interior?', 5, NULL, NULL, NULL);
INSERT INTO api_preguntas_ocasiones (descripcion, ocasiones_id, deleted_at, created_at, updated_at) VALUES ('La Recepción es en el Exterior?', 5, NULL, NULL, NULL);
INSERT INTO api_preguntas_ocasiones (descripcion, ocasiones_id, deleted_at, created_at, updated_at) VALUES ('Diurno', 3, NULL, NULL, NULL);
INSERT INTO api_preguntas_ocasiones (descripcion, ocasiones_id, deleted_at, created_at, updated_at) VALUES ('Nocturno', 3, NULL, NULL, NULL);


--
-- TOC entry 2277 (class 0 OID 17030)
-- Dependencies: 177
-- Data for Name: api_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO api_usuario (id, usuario, password, sexo, id_piel, correo, telefono, created_at, updated_at, deleted_at) VALUES (1, 'prueba', '$2y$10$iptMDCmaSWyM7gcSq0bmfOefoIqcw7BDfJjGj9ti7QL0dFcVGAyPy', 'm', 1, 'prueba@gmail.com', '0414-123-1234', '2017-04-19 23:23:05', '2017-04-19 23:23:05', NULL);


--
-- TOC entry 2363 (class 0 OID 0)
-- Dependencies: 178
-- Name: api_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_usuario_id_seq', 1, true);


--
-- TOC entry 2279 (class 0 OID 17036)
-- Dependencies: 179
-- Data for Name: app_historico; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (1, 'autenticacion', 'autenticacion', 'Clave:admin', 'admin', '2017-04-19 23:22:51', '2017-04-19 23:22:51');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (2, 'app_perfil', 'creado', '1', 'Invitado', '2017-04-19 23:23:02', '2017-04-19 23:23:02');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (3, 'app_perfil', 'creado', '2', 'Invitado', '2017-04-19 23:23:02', '2017-04-19 23:23:02');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (4, 'app_perfil', 'creado', '3', 'Invitado', '2017-04-19 23:23:02', '2017-04-19 23:23:02');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (5, 'app_perfil', 'creado', '4', 'Invitado', '2017-04-19 23:23:02', '2017-04-19 23:23:02');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (6, 'app_perfil', 'creado', '5', 'Invitado', '2017-04-19 23:23:02', '2017-04-19 23:23:02');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (7, 'app_perfil', 'creado', '6', 'Invitado', '2017-04-19 23:23:02', '2017-04-19 23:23:02');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (8, 'app_perfil', 'creado', '7', 'Invitado', '2017-04-19 23:23:02', '2017-04-19 23:23:02');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (9, 'app_usuario', 'creado', '1', 'Invitado', '2017-04-19 23:23:02', '2017-04-19 23:23:02');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (10, 'api_usuario', 'creado', '1', 'Invitado', '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (11, 'cat_colores', 'creado', '1', 'Invitado', '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (12, 'cat_colores', 'creado', '2', 'Invitado', '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (13, 'cat_colores', 'creado', '3', 'Invitado', '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (14, 'estaciones', 'creado', '1', 'Invitado', '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (15, 'estaciones', 'creado', '2', 'Invitado', '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (16, 'estaciones', 'creado', '3', 'Invitado', '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (17, 'estaciones', 'creado', '4', 'Invitado', '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (18, 'ocasiones', 'creado', '1', 'Invitado', '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (19, 'ocasiones', 'creado', '2', 'Invitado', '2017-04-19 23:23:06', '2017-04-19 23:23:06');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (20, 'ocasiones', 'creado', '3', 'Invitado', '2017-04-19 23:23:06', '2017-04-19 23:23:06');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (21, 'ocasiones', 'creado', '4', 'Invitado', '2017-04-19 23:23:06', '2017-04-19 23:23:06');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (22, 'tono_piel', 'creado', '1', 'Invitado', '2017-04-19 23:23:06', '2017-04-19 23:23:06');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (23, 'tono_piel', 'creado', '2', 'Invitado', '2017-04-19 23:23:06', '2017-04-19 23:23:06');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (24, 'tipo_prenda', 'creado', '1', 'Invitado', '2017-04-19 23:23:06', '2017-04-19 23:23:06');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (25, 'autenticacion', 'autenticacion', '', 'admin', '2017-04-19 23:23:08', '2017-04-19 23:23:08');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (26, 'api_estilos', 'creado', '1', 'admin', '2017-04-19 23:28:34', '2017-04-19 23:28:34');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (27, 'api_estilos', 'creado', '2', 'admin', '2017-04-19 23:29:03', '2017-04-19 23:29:03');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (28, 'api_estilos', 'creado', '3', 'admin', '2017-04-19 23:29:20', '2017-04-19 23:29:20');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (29, 'api_estilos', 'creado', '4', 'admin', '2017-04-19 23:29:37', '2017-04-19 23:29:37');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (30, 'api_estilos', 'creado', '5', 'admin', '2017-04-19 23:29:56', '2017-04-19 23:29:56');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (31, 'api_estilos', 'creado', '6', 'admin', '2017-04-19 23:30:36', '2017-04-19 23:30:36');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (32, 'api_estilos', 'creado', '7', 'admin', '2017-04-19 23:30:50', '2017-04-19 23:30:50');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (33, 'api_estilos', 'creado', '8', 'admin', '2017-04-19 23:31:05', '2017-04-19 23:31:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (34, 'api_estilos', 'creado', '9', 'admin', '2017-04-19 23:31:20', '2017-04-19 23:31:20');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (35, 'api_estilos', 'creado', '10', 'admin', '2017-04-19 23:31:36', '2017-04-19 23:31:36');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (36, 'api_estilos', 'creado', '11', 'admin', '2017-04-19 23:31:49', '2017-04-19 23:31:49');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (37, 'api_estilos', 'creado', '12', 'admin', '2017-04-19 23:32:08', '2017-04-19 23:32:08');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (38, 'api_estilos', 'creado', '13', 'admin', '2017-04-19 23:32:25', '2017-04-19 23:32:25');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (39, 'api_estilos', 'creado', '14', 'admin', '2017-04-19 23:32:40', '2017-04-19 23:32:40');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (40, 'api_estilos', 'creado', '15', 'admin', '2017-04-19 23:32:55', '2017-04-19 23:32:55');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (41, 'api_estilos', 'creado', '16', 'admin', '2017-04-19 23:33:10', '2017-04-19 23:33:10');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (42, 'api_estilos', 'creado', '17', 'admin', '2017-04-19 23:33:34', '2017-04-19 23:33:34');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (43, 'api_preguntas_estilos', 'creado', '1', 'admin', '2017-04-19 23:48:36', '2017-04-19 23:48:36');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (44, 'api_preguntas_estilos', 'creado', '2', 'admin', '2017-04-19 23:59:33', '2017-04-19 23:59:33');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (45, 'api_preguntas_estilos', 'eliminado', '1', 'admin', '2017-04-19 23:59:47', '2017-04-19 23:59:47');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (46, 'api_preguntas_estilos', 'creado', '3', 'admin', '2017-04-20 00:00:56', '2017-04-20 00:00:56');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (47, 'api_preguntas_estilos', 'creado', '4', 'admin', '2017-04-20 00:03:11', '2017-04-20 00:03:11');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (48, 'api_preguntas_estilos', 'creado', '5', 'admin', '2017-04-20 00:04:44', '2017-04-20 00:04:44');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (49, 'api_preguntas_estilos', 'creado', '6', 'admin', '2017-04-20 00:05:28', '2017-04-20 00:05:28');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (50, 'api_preguntas_estilos', 'creado', '7', 'admin', '2017-04-20 00:11:19', '2017-04-20 00:11:19');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (51, 'api_preguntas_estilos', 'actualizado', '7', 'admin', '2017-04-20 00:13:22', '2017-04-20 00:13:22');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (52, 'api_preguntas_estilos', 'creado', '8', 'admin', '2017-04-20 00:14:38', '2017-04-20 00:14:38');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (53, 'autenticacion', 'autenticacion', 'Clave:ME2wJTb8', 'admin', '2017-04-25 23:28:18', '2017-04-25 23:28:18');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (54, 'autenticacion', 'autenticacion', '', 'admin', '2017-04-25 23:28:27', '2017-04-25 23:28:27');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (55, 'app_usuario', 'actualizado', '1', 'admin', '2017-04-25 23:29:37', '2017-04-25 23:29:37');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (56, 'autenticacion', 'autenticacion', '', 'admin', '2017-04-25 23:29:44', '2017-04-25 23:29:44');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (57, 'app_usuario', 'actualizado', '1', 'admin', '2017-04-25 23:29:47', '2017-04-25 23:29:47');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (58, 'autenticacion', 'autenticacion', '', 'admin', '2017-04-25 23:40:58', '2017-04-25 23:40:58');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (59, 'app_usuario', 'actualizado', '1', 'admin', '2017-04-25 23:41:11', '2017-04-25 23:41:11');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (60, 'autenticacion', 'autenticacion', '', 'admin', '2017-04-25 23:42:09', '2017-04-25 23:42:09');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (61, 'app_usuario', 'actualizado', '1', 'admin', '2017-04-25 23:42:12', '2017-04-25 23:42:12');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (62, 'autenticacion', 'autenticacion', '', 'admin', '2017-04-25 23:44:18', '2017-04-25 23:44:18');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (63, 'app_usuario', 'actualizado', '1', 'admin', '2017-04-26 00:11:59', '2017-04-26 00:11:59');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (64, 'autenticacion', 'autenticacion', 'Clave:admin', 'admin', '2017-04-30 21:55:22', '2017-04-30 21:55:22');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (65, 'autenticacion', 'autenticacion', 'Clave:admin', 'admin', '2017-04-30 21:55:28', '2017-04-30 21:55:28');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (66, 'autenticacion', 'autenticacion', '', 'admin', '2017-04-30 21:56:32', '2017-04-30 21:56:32');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (67, 'texturaprenda', 'creado', '1', 'admin', '2017-04-30 22:36:47', '2017-04-30 22:36:47');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (68, 'texturaprenda', 'creado', '2', 'admin', '2017-04-30 22:36:57', '2017-04-30 22:36:57');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (69, 'texturaprenda', 'creado', '3', 'admin', '2017-04-30 22:37:10', '2017-04-30 22:37:10');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (70, 'texturaprenda', 'creado', '4', 'admin', '2017-04-30 22:37:19', '2017-04-30 22:37:19');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (71, 'tipo_prenda', 'creado', '2', 'admin', '2017-04-30 22:39:07', '2017-04-30 22:39:07');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (72, 'tipo_prenda', 'actualizado', '1', 'admin', '2017-04-30 22:40:02', '2017-04-30 22:40:02');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (73, 'tipo_prenda', 'creado', '3', 'admin', '2017-04-30 22:40:36', '2017-04-30 22:40:36');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (74, 'tipo_prenda', 'creado', '4', 'admin', '2017-04-30 22:41:07', '2017-04-30 22:41:07');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (75, 'ocasiones', 'actualizado', '4', 'admin', '2017-04-30 22:42:28', '2017-04-30 22:42:28');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (76, 'ocasiones', 'creado', '5', 'admin', '2017-04-30 22:42:42', '2017-04-30 22:42:42');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (77, 'tipo_prenda', 'actualizado', '1', 'admin', '2017-04-30 22:48:07', '2017-04-30 22:48:07');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (78, 'tipo_prenda', 'actualizado', '2', 'admin', '2017-04-30 22:48:21', '2017-04-30 22:48:21');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (79, 'tipo_prenda', 'actualizado', '4', 'admin', '2017-04-30 22:46:50', '2017-04-30 22:46:50');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (80, 'tipo_prenda', 'actualizado', '3', 'admin', '2017-04-30 22:47:20', '2017-04-30 22:47:20');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (81, 'ocasiones', 'actualizado', '5', 'admin', '2017-04-30 23:35:53', '2017-04-30 23:35:53');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (82, 'ocasiones', 'actualizado', '5', 'admin', '2017-04-30 23:41:55', '2017-04-30 23:41:55');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (83, 'ocasiones', 'actualizado', '5', 'admin', '2017-04-30 23:42:28', '2017-04-30 23:42:28');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (84, 'ocasiones', 'actualizado', '5', 'admin', '2017-05-01 00:18:26', '2017-05-01 00:18:26');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (85, 'ocasiones', 'actualizado', '3', 'admin', '2017-05-01 00:19:56', '2017-05-01 00:19:56');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (86, 'tipo_prenda', 'creado', '5', 'admin', '2017-05-01 00:48:32', '2017-05-01 00:48:32');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (87, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-01 00:49:34', '2017-05-01 00:49:34');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (88, 'autenticacion', 'autenticacion', 'Clave:admin', 'admin', '2017-05-02 20:57:52', '2017-05-02 20:57:52');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (89, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-02 20:58:28', '2017-05-02 20:58:28');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (90, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-02 21:00:31', '2017-05-02 21:00:31');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (91, 'autenticacion', 'autenticacion', 'Clave:admin', 'admin', '2017-05-02 21:00:37', '2017-05-02 21:00:37');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (92, 'autenticacion', 'autenticacion', 'Clave:admin', 'admin', '2017-05-02 21:00:40', '2017-05-02 21:00:40');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (93, 'autenticacion', 'autenticacion', 'Clave:admin', 'admin', '2017-05-02 21:00:52', '2017-05-02 21:00:52');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (94, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-02 21:00:55', '2017-05-02 21:00:55');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (95, 'autenticacion', 'autenticacion', 'Clave:admin', 'admin', '2017-05-06 19:12:44', '2017-05-06 19:12:44');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (96, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-06 19:14:47', '2017-05-06 19:14:47');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (97, 'telas', 'creado', '1', 'admin', '2017-05-06 19:17:30', '2017-05-06 19:17:30');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (98, 'telas', 'actualizado', '1', 'admin', '2017-05-06 19:18:09', '2017-05-06 19:18:09');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (99, 'telas', 'eliminado', '1', 'admin', '2017-05-06 19:34:50', '2017-05-06 19:34:50');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (100, 'turnos', 'creado', '1', 'admin', '2017-05-06 19:57:16', '2017-05-06 19:57:16');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (101, 'turnos', 'creado', '2', 'admin', '2017-05-06 19:57:27', '2017-05-06 19:57:27');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (102, 'turnos', 'creado', '3', 'admin', '2017-05-06 19:57:40', '2017-05-06 19:57:40');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (103, 'tipo_prenda', 'actualizado', '3', 'admin', '2017-05-06 20:02:34', '2017-05-06 20:02:34');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (104, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-06 20:49:36', '2017-05-06 20:49:36');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (105, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-07 17:53:44', '2017-05-07 17:53:44');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (106, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-07 19:45:01', '2017-05-07 19:45:01');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (107, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-07 21:26:35', '2017-05-07 21:26:35');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (108, 'colores_imponen_estacion', 'creado', '1', 'admin', '2017-05-07 22:55:06', '2017-05-07 22:55:06');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (109, 'tono_piel', 'creado', '3', 'admin', '2017-05-07 23:23:00', '2017-05-07 23:23:00');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (110, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-10 19:23:31', '2017-05-10 19:23:31');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (111, 'config_combinaciones', 'creado', '1', 'admin', '2017-05-10 19:25:30', '2017-05-10 19:25:30');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (112, 'tipo_prenda', 'actualizado', '5', 'admin', '2017-05-10 19:30:46', '2017-05-10 19:30:46');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (113, 'config_combinaciones', 'creado', '2', 'admin', '2017-05-10 19:50:31', '2017-05-10 19:50:31');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (114, 'config_combinaciones', 'actualizado', '2', 'admin', '2017-05-10 19:51:29', '2017-05-10 19:51:29');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (115, 'config_combinaciones', 'actualizado', '2', 'admin', '2017-05-10 19:51:59', '2017-05-10 19:51:59');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (116, 'tipo_prenda_detalle', 'creado', '1', 'admin', '2017-05-10 20:28:12', '2017-05-10 20:28:12');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (117, 'tipo_prenda_detalle', 'creado', '2', 'admin', '2017-05-10 20:28:27', '2017-05-10 20:28:27');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (118, 'tipo_prenda_detalle', 'creado', '3', 'admin', '2017-05-10 20:28:49', '2017-05-10 20:28:49');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (119, 'tipo_prenda_detalle', 'creado', '4', 'admin', '2017-05-10 20:29:23', '2017-05-10 20:29:23');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (120, 'tipo_prenda_detalle', 'creado', '5', 'admin', '2017-05-10 20:31:15', '2017-05-10 20:31:15');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (121, 'tipo_prenda_detalle', 'creado', '6', 'admin', '2017-05-10 20:31:40', '2017-05-10 20:31:40');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (122, 'tipo_prenda_detalle', 'creado', '7', 'admin', '2017-05-10 20:31:53', '2017-05-10 20:31:53');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (123, 'tipo_prenda_detalle', 'creado', '8', 'admin', '2017-05-10 20:32:56', '2017-05-10 20:32:56');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (124, 'tipo_prenda_detalle', 'creado', '9', 'admin', '2017-05-10 20:33:11', '2017-05-10 20:33:11');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (125, 'tipo_prenda_detalle', 'actualizado', '9', 'admin', '2017-05-10 20:33:35', '2017-05-10 20:33:35');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (126, 'tipo_prenda_detalle', 'actualizado', '8', 'admin', '2017-05-10 20:33:49', '2017-05-10 20:33:49');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (127, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-11 07:53:18', '2017-05-11 07:53:18');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (128, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-11 08:18:16', '2017-05-11 08:18:16');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (129, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-11 08:59:51', '2017-05-11 08:59:51');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (130, 'ocasiones', 'eliminado', '3', 'admin', '2017-05-11 10:19:27', '2017-05-11 10:19:27');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (131, 'tipo_prenda_detalle', 'creado', '10', 'admin', '2017-05-11 10:54:05', '2017-05-11 10:54:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (132, 'tipo_prenda_detalle', 'creado', '11', 'admin', '2017-05-11 10:54:34', '2017-05-11 10:54:34');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (133, 'tipo_prenda_detalle', 'creado', '12', 'admin', '2017-05-11 10:55:30', '2017-05-11 10:55:30');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (134, 'tipo_prenda_detalle', 'creado', '13', 'admin', '2017-05-11 10:55:40', '2017-05-11 10:55:40');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (135, 'tipo_prenda_detalle', 'creado', '14', 'admin', '2017-05-11 10:56:30', '2017-05-11 10:56:30');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (136, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-11 14:32:43', '2017-05-11 14:32:43');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (137, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-11 14:33:26', '2017-05-11 14:33:26');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (138, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-11 15:25:59', '2017-05-11 15:25:59');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (139, 'config_combinaciones', 'creado', '3', 'admin', '2017-05-11 15:46:17', '2017-05-11 15:46:17');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (140, 'config_combinaciones', 'creado', '4', 'admin', '2017-05-11 15:51:51', '2017-05-11 15:51:51');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (141, 'config_combinaciones', 'creado', '5', 'admin', '2017-05-11 15:58:14', '2017-05-11 15:58:14');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (142, 'config_combinaciones', 'creado', '6', 'admin', '2017-05-11 15:59:12', '2017-05-11 15:59:12');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (143, 'config_combinaciones', 'creado', '7', 'admin', '2017-05-11 15:59:33', '2017-05-11 15:59:33');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (144, 'config_combinaciones', 'creado', '8', 'admin', '2017-05-11 16:00:59', '2017-05-11 16:00:59');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (145, 'config_combinaciones', 'creado', '9', 'admin', '2017-05-11 16:02:36', '2017-05-11 16:02:36');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (146, 'config_combinaciones', 'creado', '10', 'admin', '2017-05-11 16:08:03', '2017-05-11 16:08:03');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (147, 'config_combinaciones', 'creado', '11', 'admin', '2017-05-11 16:11:13', '2017-05-11 16:11:13');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (148, 'config_combinaciones', 'actualizado', '11', 'admin', '2017-05-11 17:00:02', '2017-05-11 17:00:02');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (149, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-11 17:04:45', '2017-05-11 17:04:45');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (150, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-11 17:05:47', '2017-05-11 17:05:47');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (151, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-11 17:08:42', '2017-05-11 17:08:42');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (152, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-11 17:25:57', '2017-05-11 17:25:57');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (153, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-11 17:26:29', '2017-05-11 17:26:29');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (154, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-12 09:38:32', '2017-05-12 09:38:32');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (155, 'config_combinaciones', 'actualizado', '11', 'admin', '2017-05-12 09:44:00', '2017-05-12 09:44:00');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (156, 'config_combinaciones', 'creado', '12', 'admin', '2017-05-12 10:12:47', '2017-05-12 10:12:47');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (157, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-12 10:15:11', '2017-05-12 10:15:11');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (158, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-12 10:17:54', '2017-05-12 10:17:54');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (159, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-12 10:20:26', '2017-05-12 10:20:26');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (160, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-15 13:13:29', '2017-05-15 13:13:29');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (161, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-15 13:14:10', '2017-05-15 13:14:10');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (162, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-15 13:33:10', '2017-05-15 13:33:10');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (163, 'config_combinaciones', 'actualizado', '12', 'admin', '2017-05-15 13:33:52', '2017-05-15 13:33:52');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (164, 'config_combinaciones', 'actualizado', '11', 'admin', '2017-05-15 14:19:10', '2017-05-15 14:19:10');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (165, 'config_combinaciones', 'actualizado', '12', 'admin', '2017-05-15 14:22:45', '2017-05-15 14:22:45');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (166, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-15 15:30:08', '2017-05-15 15:30:08');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (167, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-15 16:42:26', '2017-05-15 16:42:26');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (168, 'config_combinaciones', 'creado', '13', 'admin', '2017-05-15 16:45:04', '2017-05-15 16:45:04');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (169, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-15 16:56:56', '2017-05-15 16:56:56');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (170, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-15 16:57:04', '2017-05-15 16:57:04');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (171, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-15 17:05:52', '2017-05-15 17:05:52');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (172, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-16 12:51:12', '2017-05-16 12:51:12');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (173, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-16 15:25:12', '2017-05-16 15:25:12');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (174, 'autenticacion', 'autenticacion', 'Clave:admin', 'adm in', '2017-05-16 16:32:43', '2017-05-16 16:32:43');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (175, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-16 16:32:51', '2017-05-16 16:32:51');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (176, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-16 16:38:16', '2017-05-16 16:38:16');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (177, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-19 07:58:22', '2017-05-19 07:58:22');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (178, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-19 07:58:43', '2017-05-19 07:58:43');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (179, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-19 15:00:26', '2017-05-19 15:00:26');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (180, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-19 15:01:07', '2017-05-19 15:01:07');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (181, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-19 15:36:14', '2017-05-19 15:36:14');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (182, 'config_combinaciones', 'actualizado', '12', 'admin', '2017-05-19 15:37:23', '2017-05-19 15:37:23');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (183, 'config_combinaciones', 'actualizado', '12', 'admin', '2017-05-19 15:37:49', '2017-05-19 15:37:49');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (184, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-19 16:10:35', '2017-05-19 16:10:35');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (185, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-20 17:25:02', '2017-05-20 17:25:02');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (186, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-20 17:25:45', '2017-05-20 17:25:45');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (187, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-20 17:25:51', '2017-05-20 17:25:51');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (188, 'config_combinaciones', 'creado', '14', 'admin', '2017-05-20 18:02:20', '2017-05-20 18:02:20');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (189, 'config_combinaciones', 'actualizado', '14', 'admin', '2017-05-20 18:04:27', '2017-05-20 18:04:27');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (190, 'config_combinaciones', 'actualizado', '14', 'admin', '2017-05-20 18:09:19', '2017-05-20 18:09:19');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (191, 'config_combinaciones', 'actualizado', '14', 'admin', '2017-05-20 18:10:15', '2017-05-20 18:10:15');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (192, 'config_combinaciones', 'actualizado', '14', 'admin', '2017-05-20 18:19:06', '2017-05-20 18:19:06');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (193, 'config_combinaciones', 'actualizado', '12', 'admin', '2017-05-20 18:19:35', '2017-05-20 18:19:35');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (194, 'config_combinaciones', 'actualizado', '11', 'admin', '2017-05-20 18:44:11', '2017-05-20 18:44:11');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (195, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-20 18:48:11', '2017-05-20 18:48:11');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (196, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-21 17:50:53', '2017-05-21 17:50:53');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (197, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-22 14:26:54', '2017-05-22 14:26:54');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (198, 'config_combinaciones', 'actualizado', '12', 'admin', '2017-05-22 14:28:25', '2017-05-22 14:28:25');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (199, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-22 21:46:34', '2017-05-22 21:46:34');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (200, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-22 21:47:50', '2017-05-22 21:47:50');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (201, 'autenticacion', 'autenticacion', 'Clave:|admin', 'admin', '2017-05-27 19:33:51', '2017-05-27 19:33:51');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (202, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-27 19:34:00', '2017-05-27 19:34:00');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (203, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 19:35:33', '2017-05-27 19:35:33');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (204, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 19:36:36', '2017-05-27 19:36:36');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (205, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 19:36:46', '2017-05-27 19:36:46');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (206, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 19:37:03', '2017-05-27 19:37:03');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (207, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-27 19:37:08', '2017-05-27 19:37:08');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (208, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 19:37:16', '2017-05-27 19:37:16');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (209, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 19:37:33', '2017-05-27 19:37:33');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (210, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 19:37:38', '2017-05-27 19:37:38');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (211, 'autenticacion', 'autenticacion', 'Clave:admin', 'admin', '2017-05-27 19:37:44', '2017-05-27 19:37:44');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (212, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-27 19:37:48', '2017-05-27 19:37:48');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (213, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 19:42:17', '2017-05-27 19:42:17');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (214, 'autenticacion', 'autenticacion', 'Clave:admin', 'admin', '2017-05-27 19:42:23', '2017-05-27 19:42:23');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (215, 'autenticacion', 'autenticacion', '', 'admin', '2017-05-27 19:42:27', '2017-05-27 19:42:27');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (216, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 19:42:37', '2017-05-27 19:42:37');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (217, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 19:42:49', '2017-05-27 19:42:49');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (218, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 19:42:54', '2017-05-27 19:42:54');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (219, 'tipo_prenda', 'creado', '6', 'admin', '2017-05-27 20:52:17', '2017-05-27 20:52:17');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (220, 'tipo_prenda_detalle', 'creado', '15', 'admin', '2017-05-27 20:53:07', '2017-05-27 20:53:07');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (221, 'tipo_prenda_detalle', 'creado', '16', 'admin', '2017-05-27 20:53:28', '2017-05-27 20:53:28');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (222, 'tipo_prenda_detalle', 'creado', '17', 'admin', '2017-05-27 20:53:46', '2017-05-27 20:53:46');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (223, 'tipo_prenda_detalle', 'creado', '18', 'admin', '2017-05-27 20:53:59', '2017-05-27 20:53:59');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (224, 'tipo_prenda', 'actualizado', '1', 'admin', '2017-05-27 20:58:46', '2017-05-27 20:58:46');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (225, 'tipo_prenda', 'actualizado', '2', 'admin', '2017-05-27 20:59:01', '2017-05-27 20:59:01');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (226, 'config_combinaciones', 'actualizado', '12', 'admin', '2017-05-27 21:16:04', '2017-05-27 21:16:04');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (227, 'telas', 'creado', '2', 'admin', '2017-05-27 21:36:07', '2017-05-27 21:36:07');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (228, 'telas', 'actualizado', '2', 'admin', '2017-05-27 21:53:05', '2017-05-27 21:53:05');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (229, 'app_usuario', 'actualizado', '1', 'admin', '2017-05-27 22:06:58', '2017-05-27 22:06:58');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (230, 'autenticacion', 'autenticacion', '', 'admin', '2017-06-06 12:28:17', '2017-06-06 12:28:17');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (231, 'app_usuario', 'actualizado', '1', 'admin', '2017-06-06 12:36:32', '2017-06-06 12:36:32');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (232, 'autenticacion', 'autenticacion', '', 'admin', '2017-06-06 13:58:19', '2017-06-06 13:58:19');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (233, 'config_combinaciones', 'creado', '15', 'admin', '2017-06-06 14:01:37', '2017-06-06 14:01:37');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (234, 'app_usuario', 'actualizado', '1', 'admin', '2017-06-06 14:09:40', '2017-06-06 14:09:40');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (235, 'autenticacion', 'autenticacion', '', 'admin', '2017-06-07 11:20:48', '2017-06-07 11:20:48');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (236, 'app_usuario', 'actualizado', '1', 'admin', '2017-06-07 11:45:23', '2017-06-07 11:45:23');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (237, 'autenticacion', 'autenticacion', '', 'admin', '2017-06-07 11:53:17', '2017-06-07 11:53:17');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (238, 'app_usuario', 'actualizado', '1', 'admin', '2017-06-07 11:54:35', '2017-06-07 11:54:35');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (239, 'autenticacion', 'autenticacion', '', 'admin', '2017-06-07 12:18:58', '2017-06-07 12:18:58');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (240, 'config_combinaciones', 'actualizado', '15', 'admin', '2017-06-07 12:27:55', '2017-06-07 12:27:55');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (241, 'config_combinaciones', 'actualizado', '15', 'admin', '2017-06-07 12:29:25', '2017-06-07 12:29:25');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (242, 'app_usuario', 'actualizado', '1', 'admin', '2017-06-07 12:30:11', '2017-06-07 12:30:11');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (243, 'autenticacion', 'autenticacion', '', 'admin', '2017-06-10 22:12:44', '2017-06-10 22:12:44');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (244, 'app_usuario', 'actualizado', '1', 'admin', '2017-06-11 00:17:13', '2017-06-11 00:17:13');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (245, 'autenticacion', 'autenticacion', '', 'admin', '2017-06-11 13:59:03', '2017-06-11 13:59:03');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (246, 'app_usuario', 'actualizado', '1', 'admin', '2017-06-11 14:20:44', '2017-06-11 14:20:44');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (247, 'autenticacion', 'autenticacion', '', 'admin', '2017-06-11 14:32:44', '2017-06-11 14:32:44');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (248, 'config_combinaciones', 'actualizado', '12', 'admin', '2017-06-11 14:33:13', '2017-06-11 14:33:13');
INSERT INTO app_historico (id, tabla, concepto, idregistro, usuario, created_at, updated_at) VALUES (249, 'app_usuario', 'actualizado', '1', 'admin', '2017-06-11 15:13:53', '2017-06-11 15:13:53');


--
-- TOC entry 2364 (class 0 OID 0)
-- Dependencies: 180
-- Name: app_historico_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('app_historico_id_seq', 249, true);


--
-- TOC entry 2281 (class 0 OID 17041)
-- Dependencies: 181
-- Data for Name: app_perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO app_perfil (id, nombre, created_at, updated_at, deleted_at) VALUES (1, 'Desarrollador', '2017-04-19 23:23:02', '2017-04-19 23:23:02', NULL);
INSERT INTO app_perfil (id, nombre, created_at, updated_at, deleted_at) VALUES (2, 'Administrador', '2017-04-19 23:23:02', '2017-04-19 23:23:02', NULL);
INSERT INTO app_perfil (id, nombre, created_at, updated_at, deleted_at) VALUES (3, 'Tecnico', '2017-04-19 23:23:02', '2017-04-19 23:23:02', NULL);
INSERT INTO app_perfil (id, nombre, created_at, updated_at, deleted_at) VALUES (4, 'Supervisor', '2017-04-19 23:23:02', '2017-04-19 23:23:02', NULL);
INSERT INTO app_perfil (id, nombre, created_at, updated_at, deleted_at) VALUES (5, 'Asistente', '2017-04-19 23:23:02', '2017-04-19 23:23:02', NULL);
INSERT INTO app_perfil (id, nombre, created_at, updated_at, deleted_at) VALUES (6, 'Secretaria', '2017-04-19 23:23:02', '2017-04-19 23:23:02', NULL);
INSERT INTO app_perfil (id, nombre, created_at, updated_at, deleted_at) VALUES (7, 'Movil', '2017-04-19 23:23:02', '2017-04-19 23:23:02', NULL);


--
-- TOC entry 2365 (class 0 OID 0)
-- Dependencies: 182
-- Name: app_perfil_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('app_perfil_id_seq', 7, true);


--
-- TOC entry 2283 (class 0 OID 17046)
-- Dependencies: 183
-- Data for Name: app_perfiles_permisos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2284 (class 0 OID 17049)
-- Dependencies: 184
-- Data for Name: app_preguntas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO app_preguntas (id, descripcion, deleted_at, created_at, updated_at) VALUES (1, 'Lugar de nacimiento de mi madre', NULL, '2017-04-19 23:23:03', '2017-04-19 23:23:03');
INSERT INTO app_preguntas (id, descripcion, deleted_at, created_at, updated_at) VALUES (2, 'Nombre de mi Primera Mascota', NULL, '2017-04-19 23:23:03', '2017-04-19 23:23:03');
INSERT INTO app_preguntas (id, descripcion, deleted_at, created_at, updated_at) VALUES (3, 'Nombre de mi Primera Novia', NULL, '2017-04-19 23:23:03', '2017-04-19 23:23:03');
INSERT INTO app_preguntas (id, descripcion, deleted_at, created_at, updated_at) VALUES (4, 'Segundo Nombre de mi madre', NULL, '2017-04-19 23:23:03', '2017-04-19 23:23:03');
INSERT INTO app_preguntas (id, descripcion, deleted_at, created_at, updated_at) VALUES (5, 'Color favorito', NULL, '2017-04-19 23:23:03', '2017-04-19 23:23:03');


--
-- TOC entry 2366 (class 0 OID 0)
-- Dependencies: 185
-- Name: app_preguntas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('app_preguntas_id_seq', 5, true);


--
-- TOC entry 2286 (class 0 OID 17054)
-- Dependencies: 186
-- Data for Name: app_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO app_usuario (id, usuario, password, dni, nombre, apellido, id_piel, correo, telefono, foto, perfil_id, autenticacion, super, sexo, edo_civil, direccion, facebook, instagram, twitter, preguntas_pri_id, preguntas_seg_id, respuesta_pri, respuesta_seg, remember_token, created_at, updated_at, deleted_at) VALUES (1, 'admin', '$2y$10$daL8W60qb/O0Pldv41yef.eJnS0UiSi5NXwkJ4Gx7Kto7IDkm2RIO', 12345678, 'Administrador', '', 1, 'admin@gmail.com', '0414-123-1234', 'admin.jpg', 1, 'B', 's', 'm', '', '', '', '', '', 1, 2, 'mathias', 'leal', 'JW1zM70VQ8qeYM2wsHulQoXTDUdplK7PoQ1Sm9NYWqUJU5l17sEhOk9RWwip', '2017-04-19 23:23:02', '2017-06-11 15:13:53', NULL);


--
-- TOC entry 2367 (class 0 OID 0)
-- Dependencies: 187
-- Name: app_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('app_usuario_id_seq', 1, true);


--
-- TOC entry 2288 (class 0 OID 17066)
-- Dependencies: 188
-- Data for Name: app_usuario_permisos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2289 (class 0 OID 17069)
-- Dependencies: 189
-- Data for Name: cat_colores; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO cat_colores (id, descripcion, deleted_at, created_at, updated_at) VALUES (1, 'Azul', NULL, '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO cat_colores (id, descripcion, deleted_at, created_at, updated_at) VALUES (2, 'Amarillo', NULL, '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO cat_colores (id, descripcion, deleted_at, created_at, updated_at) VALUES (3, 'Anaranjado', NULL, '2017-04-19 23:23:05', '2017-04-19 23:23:05');


--
-- TOC entry 2368 (class 0 OID 0)
-- Dependencies: 190
-- Name: cat_colores_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cat_colores_id_seq', 3, true);


--
-- TOC entry 2291 (class 0 OID 17074)
-- Dependencies: 191
-- Data for Name: colores_hexa; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2369 (class 0 OID 0)
-- Dependencies: 192
-- Name: colores_hexa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('colores_hexa_id_seq', 1, false);


--
-- TOC entry 2293 (class 0 OID 17079)
-- Dependencies: 193
-- Data for Name: colores_imponen_estacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO colores_imponen_estacion (id, descripcion, estaciones_id, deleted_at, created_at, updated_at) VALUES (1, '#c48f8f', 1, NULL, '2017-05-07 22:55:06', '2017-05-07 22:55:06');


--
-- TOC entry 2370 (class 0 OID 0)
-- Dependencies: 194
-- Name: colores_imponen_estacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('colores_imponen_estacion_id_seq', 1, true);


--
-- TOC entry 2295 (class 0 OID 17084)
-- Dependencies: 195
-- Data for Name: config_colores_prendas_princ; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (13, '#ffffff', '255', '255', '255');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (13, '#e7cccc', '231', '204', '204');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (14, '#831919', '131', '25', '25');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (11, '#fffbfb', '255', '251', '251');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (11, '#fcf0f0', '252', '240', '240');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (11, '#250c0c', '37', '12', '12');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (15, '#2e2020', '46', '32', '32');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (12, '#fffbfb', '255', '251', '251');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (12, '#fffbfb', '255', '251', '251');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (12, '#e5caca', '229', '202', '202');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (12, '#ffffff', '255', '255', '255');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (12, '#fff1f1', '255', '241', '241');
INSERT INTO config_colores_prendas_princ (config_combina_id, hexadecimal, r, g, b) VALUES (12, '#100606', '16', '6', '6');


--
-- TOC entry 2296 (class 0 OID 17087)
-- Dependencies: 196
-- Data for Name: config_colores_prendas_sec; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (13, 2, '#1f1313', '31', '19', '19');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (13, 3, '#080404', '8', '4', '4');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (13, 4, '#080404', '8', '4', '4');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (14, 2, '#060303', '6', '3', '3');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (14, 2, '#fefefe', '254', '254', '254');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (14, 2, '#5c4b4b', '92', '75', '75');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (14, 3, '#0e0505', '14', '5', '5');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (14, 3, '#ffffff', '255', '255', '255');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (14, 4, '#000000', '0', '0', '0');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (11, 2, '#2146d7', '33', '70', '215');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (11, 2, '#00cdff', '0', '205', '255');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (11, 3, '#130e0e', '19', '14', '14');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (11, 3, '#fbfbfb', '251', '251', '251');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (11, 4, '#0d0909', '13', '9', '9');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (11, 4, '#aeaeae', '174', '174', '174');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (15, 1, '#211717', '33', '23', '23');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (15, 3, '#cf7878', '207', '120', '120');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (15, 4, '#8c8282', '140', '130', '130');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (15, 6, '#080202', '8', '2', '2');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (15, 6, '#f0cccc', '240', '204', '204');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (12, 2, '#fef2f2', '254', '242', '242');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (12, 2, '#ffffff', '255', '255', '255');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (12, 3, '#161010', '22', '16', '16');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (12, 3, '#ffffff', '255', '255', '255');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (12, 4, '#f6e4e4', '246', '228', '228');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (12, 4, '#0c0505', '12', '5', '5');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (12, 4, '#fcfcfc', '252', '252', '252');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (12, 6, '#050303', '5', '3', '3');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (12, 6, '#f7f7f7', '247', '247', '247');
INSERT INTO config_colores_prendas_sec (config_combina_id, tipo_prenda_id, hexadecimal, r, g, b) VALUES (12, 6, '#000d4f', '0', '13', '79');


--
-- TOC entry 2297 (class 0 OID 17090)
-- Dependencies: 197
-- Data for Name: config_combinaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO config_combinaciones (id, prenda_princ_id, descripcion, created_at, deleted_at, updated_at) VALUES (13, 1, 'TERCERA', '16:45:04-04:30', NULL, '16:45:04-04:30');
INSERT INTO config_combinaciones (id, prenda_princ_id, descripcion, created_at, deleted_at, updated_at) VALUES (14, 1, 'CUARTA COMBINACION', '18:02:20-04:30', NULL, '18:19:06-04:30');
INSERT INTO config_combinaciones (id, prenda_princ_id, descripcion, created_at, deleted_at, updated_at) VALUES (11, 1, 'PRIMERA CONFIGURACION', '16:11:13-04:30', NULL, '18:44:11-04:30');
INSERT INTO config_combinaciones (id, prenda_princ_id, descripcion, created_at, deleted_at, updated_at) VALUES (15, 2, 'RUMIN', '14:01:37-04:30', NULL, '12:29:25-04:30');
INSERT INTO config_combinaciones (id, prenda_princ_id, descripcion, created_at, deleted_at, updated_at) VALUES (12, 1, 'COMBINACIÓN GALA', '10:12:47-04:30', NULL, '14:33:13-04:30');


--
-- TOC entry 2371 (class 0 OID 0)
-- Dependencies: 198
-- Name: config_combinaciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('config_combinaciones_id_seq', 15, true);


--
-- TOC entry 2299 (class 0 OID 17095)
-- Dependencies: 199
-- Data for Name: config_estacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 4);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 4);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 4);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 4);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (13, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (13, 2);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (13, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (13, 4);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 4);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 4);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 4);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 4);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (14, 4);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (11, 4);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (15, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (15, 2);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (12, 1);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (12, 3);
INSERT INTO config_estacion (config_combina_id, estaciones_id) VALUES (12, 4);


--
-- TOC entry 2300 (class 0 OID 17098)
-- Dependencies: 200
-- Data for Name: config_ocasiones; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (13, 1);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (13, 2);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (14, 1);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (14, 2);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (14, 4);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (14, 5);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (11, 1);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (11, 2);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (15, 1);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (15, 2);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (15, 4);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (12, 2);
INSERT INTO config_ocasiones (config_combina_id, ocasiones_id) VALUES (12, 4);


--
-- TOC entry 2301 (class 0 OID 17101)
-- Dependencies: 201
-- Data for Name: config_sexo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO config_sexo (config_combina_id, sexo) VALUES (13, 'f');
INSERT INTO config_sexo (config_combina_id, sexo) VALUES (13, 'm');
INSERT INTO config_sexo (config_combina_id, sexo) VALUES (14, 'f');
INSERT INTO config_sexo (config_combina_id, sexo) VALUES (14, 'm');
INSERT INTO config_sexo (config_combina_id, sexo) VALUES (11, 'm');
INSERT INTO config_sexo (config_combina_id, sexo) VALUES (15, 'f');
INSERT INTO config_sexo (config_combina_id, sexo) VALUES (15, 'm');
INSERT INTO config_sexo (config_combina_id, sexo) VALUES (12, 'f');
INSERT INTO config_sexo (config_combina_id, sexo) VALUES (12, 'm');


--
-- TOC entry 2302 (class 0 OID 17104)
-- Dependencies: 202
-- Data for Name: config_tipos_prendas_princ_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (11, 1, 1);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (11, 1, 2);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (13, 1, 1);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (13, 1, 2);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (13, 1, 3);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (13, 1, 4);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (14, 1, 1);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (14, 1, 2);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (14, 1, 3);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (14, 1, 4);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (15, 2, 5);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (15, 2, 6);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (15, 2, 7);
INSERT INTO config_tipos_prendas_princ_detalle (config_combina_id, prenda_princ_id, tipo_prenda_detalle_id) VALUES (12, 1, 4);


--
-- TOC entry 2303 (class 0 OID 17107)
-- Dependencies: 203
-- Data for Name: config_tipos_prendas_sec_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (13, 2, 5);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (13, 2, 6);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (13, 2, 7);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (13, 3, 12);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (13, 3, 13);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (13, 3, 14);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (13, 4, 10);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (13, 4, 11);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (14, 2, 6);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (14, 2, 7);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (14, 3, 13);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (14, 3, 14);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (14, 4, 10);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (14, 4, 11);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (11, 2, 5);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (11, 2, 6);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (11, 3, 13);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (11, 4, 11);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (15, 1, 1);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (15, 1, 2);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (15, 3, 12);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (15, 3, 13);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (15, 4, 10);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (15, 4, 11);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (15, 6, 15);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (15, 6, 16);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (15, 6, 17);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (15, 6, 18);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (12, 2, 7);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (12, 3, 14);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (12, 4, 10);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (12, 4, 11);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (12, 6, 17);
INSERT INTO config_tipos_prendas_sec_detalle (config_combina_id, prenda_sec_id, tipo_prenda_detalle_id) VALUES (12, 6, 18);


--
-- TOC entry 2304 (class 0 OID 17110)
-- Dependencies: 204
-- Data for Name: config_tono_piel; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (13, 1);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (13, 2);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (13, 3);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (14, 1);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (14, 2);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (14, 3);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (11, 1);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (11, 2);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (11, 3);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (15, 1);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (15, 2);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (15, 3);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (12, 1);
INSERT INTO config_tono_piel (config_combina_id, tono_piel_id) VALUES (12, 2);


--
-- TOC entry 2305 (class 0 OID 17113)
-- Dependencies: 205
-- Data for Name: estaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO estaciones (id, descripcion, estatus, deleted_at, created_at, updated_at) VALUES (1, 'Verano', '1', NULL, '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO estaciones (id, descripcion, estatus, deleted_at, created_at, updated_at) VALUES (2, 'Invierno', '0', NULL, '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO estaciones (id, descripcion, estatus, deleted_at, created_at, updated_at) VALUES (3, 'Primavera', '0', NULL, '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO estaciones (id, descripcion, estatus, deleted_at, created_at, updated_at) VALUES (4, 'Otoño', '0', NULL, '2017-04-19 23:23:05', '2017-04-19 23:23:05');


--
-- TOC entry 2372 (class 0 OID 0)
-- Dependencies: 206
-- Name: estaciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('estaciones_id_seq', 4, true);


--
-- TOC entry 2307 (class 0 OID 17118)
-- Dependencies: 207
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO migrations (id, migration, batch) VALUES (1, '2016_10_04_092235_sessions', 1);
INSERT INTO migrations (id, migration, batch) VALUES (2, '2016_10_04_092243_password_resets', 1);
INSERT INTO migrations (id, migration, batch) VALUES (3, '2016_10_04_092257_app_perfil', 1);
INSERT INTO migrations (id, migration, batch) VALUES (4, '2016_10_04_092308_app_usuario', 1);
INSERT INTO migrations (id, migration, batch) VALUES (5, '2016_10_04_092315_app_perfiles_permisos', 1);
INSERT INTO migrations (id, migration, batch) VALUES (6, '2016_10_04_092321_app_usuario_permisos', 1);
INSERT INTO migrations (id, migration, batch) VALUES (7, '2016_10_04_092327_app_historico', 1);
INSERT INTO migrations (id, migration, batch) VALUES (8, '2017_04_07_142339_preguntas', 1);
INSERT INTO migrations (id, migration, batch) VALUES (9, '2017_03_30_134901_ocasiones', 2);
INSERT INTO migrations (id, migration, batch) VALUES (10, '2017_03_30_135059_tipo_prenda', 2);
INSERT INTO migrations (id, migration, batch) VALUES (11, '2017_03_31_101945_cat_colores', 2);
INSERT INTO migrations (id, migration, batch) VALUES (12, '2017_03_31_213739_Estaciones', 2);
INSERT INTO migrations (id, migration, batch) VALUES (13, '2017_03_31_222153_TonoPiel', 2);
INSERT INTO migrations (id, migration, batch) VALUES (14, '2017_04_01_200616_PrendaUsuario', 2);
INSERT INTO migrations (id, migration, batch) VALUES (15, '2017_04_03_230652_ColoresPrendaUsuario', 2);
INSERT INTO migrations (id, migration, batch) VALUES (16, '2017_04_03_231145_ColoresHexa', 2);
INSERT INTO migrations (id, migration, batch) VALUES (17, '2017_04_03_232418_ColoresImponenEstacion', 2);
INSERT INTO migrations (id, migration, batch) VALUES (18, '2017_04_09_220239_ApiUsuario', 2);
INSERT INTO migrations (id, migration, batch) VALUES (19, '2017_04_19_230030_api_estilos', 2);
INSERT INTO migrations (id, migration, batch) VALUES (20, '2017_04_19_230033_api_preguntas_estilo', 2);


--
-- TOC entry 2373 (class 0 OID 0)
-- Dependencies: 208
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('migrations_id_seq', 20, true);


--
-- TOC entry 2309 (class 0 OID 17123)
-- Dependencies: 209
-- Data for Name: ocasiones; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ocasiones (id, descripcion, deleted_at, created_at, updated_at) VALUES (1, 'Deportivo', NULL, '2017-04-19 23:23:05', '2017-04-19 23:23:05');
INSERT INTO ocasiones (id, descripcion, deleted_at, created_at, updated_at) VALUES (2, 'Fiesta', NULL, '2017-04-19 23:23:06', '2017-04-19 23:23:06');
INSERT INTO ocasiones (id, descripcion, deleted_at, created_at, updated_at) VALUES (4, 'Fiesta Gala', NULL, '2017-04-19 23:23:06', '2017-04-30 22:42:28');
INSERT INTO ocasiones (id, descripcion, deleted_at, created_at, updated_at) VALUES (5, 'Tipo Coctel', NULL, '2017-04-30 22:42:42', '2017-05-01 00:18:26');
INSERT INTO ocasiones (id, descripcion, deleted_at, created_at, updated_at) VALUES (3, 'Gala', '2017-05-11 10:19:27', '2017-04-19 23:23:06', '2017-05-01 00:19:56');


--
-- TOC entry 2374 (class 0 OID 0)
-- Dependencies: 210
-- Name: ocasiones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ocasiones_id_seq', 5, true);


--
-- TOC entry 2311 (class 0 OID 17128)
-- Dependencies: 211
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2312 (class 0 OID 17134)
-- Dependencies: 212
-- Data for Name: prenda_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO prenda_usuario (id, usuario_id, url, favorito, tipo_prenda_id, created_at, updated_at, deleted_at, tipo_prenda_detalle_id, hexadecimal, r, g, b) VALUES (2, 1, 'camara/100420172.jpg', false, 1, NULL, NULL, NULL, 1, '#ff0000', '255', '241', '237');
INSERT INTO prenda_usuario (id, usuario_id, url, favorito, tipo_prenda_id, created_at, updated_at, deleted_at, tipo_prenda_detalle_id, hexadecimal, r, g, b) VALUES (3, 1, 'camara/1524.jpg', true, 1, NULL, NULL, NULL, 1, '#ffffff', '255', '255', '255');
INSERT INTO prenda_usuario (id, usuario_id, url, favorito, tipo_prenda_id, created_at, updated_at, deleted_at, tipo_prenda_detalle_id, hexadecimal, r, g, b) VALUES (5, 1, 'camara/2525.jpg', false, 1, NULL, NULL, NULL, 4, '#070606', '7', '6', '6');
INSERT INTO prenda_usuario (id, usuario_id, url, favorito, tipo_prenda_id, created_at, updated_at, deleted_at, tipo_prenda_detalle_id, hexadecimal, r, g, b) VALUES (6, 1, 'camara/pantalongala.jpg', true, 2, NULL, NULL, NULL, 7, '#ffffff', '255', '255', '255');
INSERT INTO prenda_usuario (id, usuario_id, url, favorito, tipo_prenda_id, created_at, updated_at, deleted_at, tipo_prenda_detalle_id, hexadecimal, r, g, b) VALUES (7, 1, 'camara/zapatoas.jpg', false, 3, NULL, NULL, NULL, 14, '#060101', '6', '1', '1');
INSERT INTO prenda_usuario (id, usuario_id, url, favorito, tipo_prenda_id, created_at, updated_at, deleted_at, tipo_prenda_detalle_id, hexadecimal, r, g, b) VALUES (8, 1, 'camara/pantalongala2.jpg', false, 2, NULL, NULL, NULL, 7, '#f2ffff', '252', '253', '255');
INSERT INTO prenda_usuario (id, usuario_id, url, favorito, tipo_prenda_id, created_at, updated_at, deleted_at, tipo_prenda_detalle_id, hexadecimal, r, g, b) VALUES (11, 1, 'camara/zapatovestir.jpg', true, 3, NULL, NULL, NULL, 14, '#021123', '1', '5', '6');
INSERT INTO prenda_usuario (id, usuario_id, url, favorito, tipo_prenda_id, created_at, updated_at, deleted_at, tipo_prenda_detalle_id, hexadecimal, r, g, b) VALUES (12, 1, 'camara/short.jpg', true, 2, NULL, NULL, NULL, 5, '#ffffff', '255', '255', '255');
INSERT INTO prenda_usuario (id, usuario_id, url, favorito, tipo_prenda_id, created_at, updated_at, deleted_at, tipo_prenda_detalle_id, hexadecimal, r, g, b) VALUES (13, 1, 'camara/sacogala.jpg', false, 6, NULL, NULL, NULL, 18, '#070606', '5', '7', '7');


--
-- TOC entry 2375 (class 0 OID 0)
-- Dependencies: 213
-- Name: prenda_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('prenda_usuario_id_seq', 13, true);


--
-- TOC entry 2314 (class 0 OID 17139)
-- Dependencies: 214
-- Data for Name: sessions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sessions (id, user_id, ip_address, user_agent, payload, last_activity) VALUES ('7iSYUqUKHTi8kN3XRP6a2fSlKwaCQX6GdELtPICk', NULL, '::1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiTFRpY3dCek4xbnB6T21LZDJ1M0h3dXhzMFB1Q1kxOXBHeVdSS3o0eiI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoyNToiaHR0cDovL2xvY2FsaG9zdC9jcmlzdGlhbiI7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjMxOiJodHRwOi8vbG9jYWxob3N0L2NyaXN0aWFuL2xvZ2luIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0OTY4NTMwMTE7czoxOiJjIjtpOjE0OTY4NDg4NDA7czoxOiJsIjtzOjE6IjAiO319', 1496853011);
INSERT INTO sessions (id, user_id, ip_address, user_agent, payload, last_activity) VALUES ('OPItMjUPn50iToMIQeeTzUQs8XodczbSdHfwRgqi', NULL, '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoib2U3eGRTbGlyQnFrYlY2T01sVGFrSmZuNHJnQUxDeFRMVDgzYlgwNSI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoyNToiaHR0cDovL2xvY2FsaG9zdC9raGFsZWVzaSI7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjMxOiJodHRwOi8vbG9jYWxob3N0L2toYWxlZXNpL2xvZ2luIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0OTcyMDg0MzM7czoxOiJjIjtpOjE0OTcyMDM5MzM7czoxOiJsIjtzOjE6IjAiO319', 1497208433);
INSERT INTO sessions (id, user_id, ip_address, user_agent, payload, last_activity) VALUES ('IQ4LFDFJQFHeVJGSDNUEiRllTXfrVW2FwFkVUGtQ', NULL, '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiUmNwRHQ5TFlUNFZTaUljN1ZDd2ExbDZGSm1Fb2xFZU5JUnBzaEMzQyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHA6Ly9sb2NhbGhvc3Qva2hhbGVlc2kvbG9naW4iO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ5NzE1NDYzNTtzOjE6ImMiO2k6MTQ5NzE0NzEzODtzOjE6ImwiO3M6MToiMCI7fX0=', 1497154635);


--
-- TOC entry 2315 (class 0 OID 17145)
-- Dependencies: 215
-- Data for Name: telas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO telas (id, descripcion, estacion_id, deleted_at, created_at, updated_at) VALUES (1, 'LINO', 1, '19:34:50-04:30', '19:17:30-04:30', '19:18:09-04:30');
INSERT INTO telas (id, descripcion, estacion_id, deleted_at, created_at, updated_at) VALUES (2, 'LINO', 2, NULL, '21:36:07-04:30', '21:53:05-04:30');


--
-- TOC entry 2376 (class 0 OID 0)
-- Dependencies: 216
-- Name: telas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('telas_id_seq', 2, true);


--
-- TOC entry 2317 (class 0 OID 17150)
-- Dependencies: 217
-- Data for Name: telas_img; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO telas_img (telas_id, url, nombre, id, deleted_at, created_at, updated_at) VALUES (2, 'img/telas/2017/2/2/z9ybjjrcjc3p5idbdw49.jpg', 'z9ybjjrcjc3p5idbdw49.jpg', 2, NULL, NULL, NULL);


--
-- TOC entry 2377 (class 0 OID 0)
-- Dependencies: 218
-- Name: telas_img_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('telas_img_id_seq', 2, true);


--
-- TOC entry 2319 (class 0 OID 17155)
-- Dependencies: 219
-- Data for Name: texturaprenda; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO texturaprenda (id, descripcion, deleted_at, updated_at, created_at) VALUES (1, 'Jeans', NULL, '2017-04-30 22:36:47-04:30', '2017-04-30 22:36:47-04:30');
INSERT INTO texturaprenda (id, descripcion, deleted_at, updated_at, created_at) VALUES (2, 'Lana', NULL, '2017-04-30 22:36:57-04:30', '2017-04-30 22:36:57-04:30');
INSERT INTO texturaprenda (id, descripcion, deleted_at, updated_at, created_at) VALUES (3, 'Algodon', NULL, '2017-04-30 22:37:10-04:30', '2017-04-30 22:37:10-04:30');
INSERT INTO texturaprenda (id, descripcion, deleted_at, updated_at, created_at) VALUES (4, 'Acurrugada', NULL, '2017-04-30 22:37:19-04:30', '2017-04-30 22:37:19-04:30');


--
-- TOC entry 2378 (class 0 OID 0)
-- Dependencies: 220
-- Name: texturaprenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('texturaprenda_id_seq', 4, true);


--
-- TOC entry 2321 (class 0 OID 17160)
-- Dependencies: 221
-- Data for Name: tipo_prenda; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipo_prenda (id, descripcion, deleted_at, created_at, updated_at) VALUES (4, 'Accesorios', NULL, '2017-04-30 22:41:07', '2017-04-30 22:46:50');
INSERT INTO tipo_prenda (id, descripcion, deleted_at, created_at, updated_at) VALUES (3, 'Calzados', NULL, '2017-04-30 22:40:36', '2017-05-06 20:02:34');
INSERT INTO tipo_prenda (id, descripcion, deleted_at, created_at, updated_at) VALUES (5, 'Vestidos', NULL, '2017-05-01 00:48:32', '2017-05-10 19:30:46');
INSERT INTO tipo_prenda (id, descripcion, deleted_at, created_at, updated_at) VALUES (6, 'Complementos Superiores', NULL, '2017-05-27 20:52:17', '2017-05-27 20:52:17');
INSERT INTO tipo_prenda (id, descripcion, deleted_at, created_at, updated_at) VALUES (1, 'Parte Superior', NULL, '2017-04-19 23:23:06', '2017-05-27 20:58:46');
INSERT INTO tipo_prenda (id, descripcion, deleted_at, created_at, updated_at) VALUES (2, 'Parte Inferior', NULL, '2017-04-30 22:39:07', '2017-05-27 20:59:01');


--
-- TOC entry 2322 (class 0 OID 17163)
-- Dependencies: 222
-- Data for Name: tipo_prenda_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (1, 1, 'FRANELAS', NULL, '20:28:12-04:30', '20:28:12-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (2, 1, 'CAMISA MANGA CORTA', NULL, '20:28:27-04:30', '20:28:27-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (3, 1, 'CAMISA MANGA LARGA CASUAL', NULL, '20:28:49-04:30', '20:28:49-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (4, 1, 'CAMISA GALA', NULL, '20:29:23-04:30', '20:29:23-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (5, 2, 'SHORS', NULL, '20:31:15-04:30', '20:31:15-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (6, 2, 'PANTALÓN CASUAL', NULL, '20:31:40-04:30', '20:31:40-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (7, 2, 'PANTALON GALA', NULL, '20:31:53-04:30', '20:31:53-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (9, 5, 'VESTIDO GALA', NULL, '20:33:11-04:30', '20:33:35-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (8, 5, 'VESTIDOS DESCOTADOS', NULL, '20:32:56-04:30', '20:33:49-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (10, 4, 'RELOJ', NULL, '10:54:05-04:30', '10:54:05-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (11, 4, 'CINTURÓN', NULL, '10:54:34-04:30', '10:54:34-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (12, 3, 'SANDALIAS', NULL, '10:55:30-04:30', '10:55:30-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (13, 3, 'BOTAS', NULL, '10:55:40-04:30', '10:55:40-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (14, 3, 'ZAPATOS', NULL, '10:56:30-04:30', '10:56:30-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (15, 6, 'SUDADERAS', NULL, '20:53:07-04:30', '20:53:07-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (16, 6, 'CHAQUETA DEPORTIVA', NULL, '20:53:28-04:30', '20:53:28-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (17, 6, 'CHAQUETA CASUAL', NULL, '20:53:46-04:30', '20:53:46-04:30');
INSERT INTO tipo_prenda_detalle (id, tipo_prenda_id, descripcion, deleted_at, created_at, updated_at) VALUES (18, 6, 'SACO GALA', NULL, '20:53:59-04:30', '20:53:59-04:30');


--
-- TOC entry 2379 (class 0 OID 0)
-- Dependencies: 223
-- Name: tipo_prenda_detalle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipo_prenda_detalle_id_seq', 18, true);


--
-- TOC entry 2380 (class 0 OID 0)
-- Dependencies: 224
-- Name: tipo_prenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipo_prenda_id_seq', 6, true);


--
-- TOC entry 2325 (class 0 OID 17170)
-- Dependencies: 225
-- Data for Name: tipos_prendas_relacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (5, 4);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (5, 3);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (6, 1);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (6, 2);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (6, 4);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (6, 3);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (6, 5);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (1, 2);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (1, 4);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (1, 3);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (1, 6);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (2, 1);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (2, 4);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (2, 3);
INSERT INTO tipos_prendas_relacion (tipo_prenda_id, tipo_prenda_id_relacion) VALUES (2, 6);


--
-- TOC entry 2326 (class 0 OID 17173)
-- Dependencies: 226
-- Data for Name: tono_piel; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tono_piel (id, descripcion, deleted_at, created_at, updated_at) VALUES (1, 'Blanca', NULL, '2017-04-19 23:23:06', '2017-04-19 23:23:06');
INSERT INTO tono_piel (id, descripcion, deleted_at, created_at, updated_at) VALUES (2, 'Morena', NULL, '2017-04-19 23:23:06', '2017-04-19 23:23:06');
INSERT INTO tono_piel (id, descripcion, deleted_at, created_at, updated_at) VALUES (3, 'Oscuras', NULL, '2017-05-07 23:23:00', '2017-05-07 23:23:00');


--
-- TOC entry 2381 (class 0 OID 0)
-- Dependencies: 227
-- Name: tono_piel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tono_piel_id_seq', 3, true);


--
-- TOC entry 2328 (class 0 OID 17178)
-- Dependencies: 228
-- Data for Name: turnos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO turnos (id, descripcion, deleted_at, updated_at, created_at) VALUES (1, 'Diurno', NULL, '19:57:16-04:30', '19:57:16-04:30');
INSERT INTO turnos (id, descripcion, deleted_at, updated_at, created_at) VALUES (2, 'Nocturno', NULL, '19:57:27-04:30', '19:57:27-04:30');
INSERT INTO turnos (id, descripcion, deleted_at, updated_at, created_at) VALUES (3, 'Tarde Noche', NULL, '19:57:40-04:30', '19:57:40-04:30');


--
-- TOC entry 2382 (class 0 OID 0)
-- Dependencies: 229
-- Name: turnos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('turnos_id_seq', 3, true);


--
-- TOC entry 2330 (class 0 OID 17299)
-- Dependencies: 230
-- Data for Name: votacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO votacion (config_combina_id, usuario_id, rating) VALUES (12, 1, 10);
INSERT INTO votacion (config_combina_id, usuario_id, rating) VALUES (12, 1, 10);
INSERT INTO votacion (config_combina_id, usuario_id, rating) VALUES (12, 1, 10);


--
-- TOC entry 2099 (class 2606 OID 17206)
-- Name: api_estilos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY api_estilos
    ADD CONSTRAINT api_estilos_pkey PRIMARY KEY (id);


--
-- TOC entry 2101 (class 2606 OID 17208)
-- Name: api_preguntas_estilos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY api_preguntas_estilos
    ADD CONSTRAINT api_preguntas_estilos_pkey PRIMARY KEY (id);


--
-- TOC entry 2103 (class 2606 OID 17210)
-- Name: api_usuario_correo_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY api_usuario
    ADD CONSTRAINT api_usuario_correo_unique UNIQUE (correo);


--
-- TOC entry 2105 (class 2606 OID 17212)
-- Name: api_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY api_usuario
    ADD CONSTRAINT api_usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2107 (class 2606 OID 17214)
-- Name: api_usuario_usuario_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY api_usuario
    ADD CONSTRAINT api_usuario_usuario_unique UNIQUE (usuario);


--
-- TOC entry 2109 (class 2606 OID 17216)
-- Name: app_historico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_historico
    ADD CONSTRAINT app_historico_pkey PRIMARY KEY (id);


--
-- TOC entry 2111 (class 2606 OID 17218)
-- Name: app_perfil_nombre_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_perfil
    ADD CONSTRAINT app_perfil_nombre_unique UNIQUE (nombre);


--
-- TOC entry 2113 (class 2606 OID 17220)
-- Name: app_perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_perfil
    ADD CONSTRAINT app_perfil_pkey PRIMARY KEY (id);


--
-- TOC entry 2115 (class 2606 OID 17222)
-- Name: app_preguntas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_preguntas
    ADD CONSTRAINT app_preguntas_pkey PRIMARY KEY (id);


--
-- TOC entry 2117 (class 2606 OID 17224)
-- Name: app_usuario_correo_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_usuario
    ADD CONSTRAINT app_usuario_correo_unique UNIQUE (correo);


--
-- TOC entry 2119 (class 2606 OID 17226)
-- Name: app_usuario_dni_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_usuario
    ADD CONSTRAINT app_usuario_dni_unique UNIQUE (dni);


--
-- TOC entry 2121 (class 2606 OID 17228)
-- Name: app_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_usuario
    ADD CONSTRAINT app_usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2123 (class 2606 OID 17230)
-- Name: app_usuario_usuario_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_usuario
    ADD CONSTRAINT app_usuario_usuario_unique UNIQUE (usuario);


--
-- TOC entry 2125 (class 2606 OID 17232)
-- Name: cat_colores_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cat_colores
    ADD CONSTRAINT cat_colores_pkey PRIMARY KEY (id);


--
-- TOC entry 2127 (class 2606 OID 17234)
-- Name: colores_hexa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY colores_hexa
    ADD CONSTRAINT colores_hexa_pkey PRIMARY KEY (id);


--
-- TOC entry 2129 (class 2606 OID 17236)
-- Name: colores_imponen_estacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY colores_imponen_estacion
    ADD CONSTRAINT colores_imponen_estacion_pkey PRIMARY KEY (id);


--
-- TOC entry 2131 (class 2606 OID 17238)
-- Name: config_combinaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY config_combinaciones
    ADD CONSTRAINT config_combinaciones_pkey PRIMARY KEY (id);


--
-- TOC entry 2133 (class 2606 OID 17240)
-- Name: estaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estaciones
    ADD CONSTRAINT estaciones_pkey PRIMARY KEY (id);


--
-- TOC entry 2135 (class 2606 OID 17242)
-- Name: migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2137 (class 2606 OID 17244)
-- Name: ocasiones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ocasiones
    ADD CONSTRAINT ocasiones_pkey PRIMARY KEY (id);


--
-- TOC entry 2141 (class 2606 OID 17246)
-- Name: prenda_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY prenda_usuario
    ADD CONSTRAINT prenda_usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2143 (class 2606 OID 17248)
-- Name: sessions_id_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT sessions_id_unique UNIQUE (id);


--
-- TOC entry 2145 (class 2606 OID 17250)
-- Name: telas_img_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY telas_img
    ADD CONSTRAINT telas_img_pkey PRIMARY KEY (id);


--
-- TOC entry 2147 (class 2606 OID 17252)
-- Name: texturaprenda_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY texturaprenda
    ADD CONSTRAINT texturaprenda_pkey PRIMARY KEY (id);


--
-- TOC entry 2151 (class 2606 OID 17254)
-- Name: tipo_prenda_detalle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipo_prenda_detalle
    ADD CONSTRAINT tipo_prenda_detalle_pkey PRIMARY KEY (id);


--
-- TOC entry 2149 (class 2606 OID 17256)
-- Name: tipo_prenda_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipo_prenda
    ADD CONSTRAINT tipo_prenda_pkey PRIMARY KEY (id);


--
-- TOC entry 2153 (class 2606 OID 17258)
-- Name: tono_piel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tono_piel
    ADD CONSTRAINT tono_piel_pkey PRIMARY KEY (id);


--
-- TOC entry 2155 (class 2606 OID 17260)
-- Name: turnos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY turnos
    ADD CONSTRAINT turnos_pkey PRIMARY KEY (id);


--
-- TOC entry 2138 (class 1259 OID 17261)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- TOC entry 2139 (class 1259 OID 17262)
-- Name: password_resets_token_index; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX password_resets_token_index ON password_resets USING btree (token);


--
-- TOC entry 2156 (class 2606 OID 17263)
-- Name: api_preguntas_estilos_estilo_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_preguntas_estilos
    ADD CONSTRAINT api_preguntas_estilos_estilo_id_foreign FOREIGN KEY (estilo_id) REFERENCES api_estilos(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2157 (class 2606 OID 17268)
-- Name: api_preguntas_ocasiones_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_preguntas_ocasiones
    ADD CONSTRAINT api_preguntas_ocasiones_id_foreign FOREIGN KEY (ocasiones_id) REFERENCES ocasiones(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2158 (class 2606 OID 17273)
-- Name: app_perfiles_permisos_perfil_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_perfiles_permisos
    ADD CONSTRAINT app_perfiles_permisos_perfil_id_foreign FOREIGN KEY (perfil_id) REFERENCES app_perfil(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2159 (class 2606 OID 17278)
-- Name: app_usuario_perfil_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_usuario
    ADD CONSTRAINT app_usuario_perfil_id_foreign FOREIGN KEY (perfil_id) REFERENCES app_perfil(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2160 (class 2606 OID 17283)
-- Name: app_usuario_permisos_usuario_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_usuario_permisos
    ADD CONSTRAINT app_usuario_permisos_usuario_id_foreign FOREIGN KEY (usuario_id) REFERENCES app_usuario(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2161 (class 2606 OID 17288)
-- Name: colores_hexa_cat_colores_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY colores_hexa
    ADD CONSTRAINT colores_hexa_cat_colores_id_foreign FOREIGN KEY (cat_colores_id) REFERENCES cat_colores(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2162 (class 2606 OID 17293)
-- Name: colores_imponen_estacion_estaciones_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY colores_imponen_estacion
    ADD CONSTRAINT colores_imponen_estacion_estaciones_id_foreign FOREIGN KEY (estaciones_id) REFERENCES estaciones(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2337 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-06-11 15:56:40

--
-- PostgreSQL database dump complete
--

